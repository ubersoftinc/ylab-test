import { _ } from 'lodash'

export const types = {
  LOAD: 'LIST/LOAD',
  LOAD_SUCCESS: 'LIST/LOAD_SUCCESS',
  UPDATE_ITEM: 'LIST/UPDATE_ITEM',
}

export default {
  load: () => {
    return async dispatch => {
      dispatch({ type: types.LOAD })
 
      const listsData = [
        { _id: 1, title: 'Список', parent: null },
        { _id: 2, title: 'Первый пункт', parent: 1 },
        { _id: 6, title: 'Вложенный подпункт 2.2.1', parent: 5 },
        { _id: 3, title: 'Второй пункт', parent: 1 },
        { _id: 4, title: 'Подпункт 2.1', parent: 3 },
        { _id: 5, title: 'Подпункт 2.2', parent: 3 },
        { _id: 7, title: 'Вложенный подпункт 2.2.2', parent: 5 },
        { _id: 8, title: 'Подпункт 2.3', parent: 3 },
        { _id: 9, title: 'Третий пункт', parent: 1 },
        { _id: 10, title: 'Четвертый пункт', parent: 1 },
        { _id: 11, title: 'Оглавление', parent: null },
        { _id: 12, title: 'Вступление', parent: 11 },
        { _id: 16, title: 'Первое блюдо', parent: 15 },
        { _id: 13, title: 'Часть первая', parent: 11 },
        { _id: 14, title: 'Затрак', parent: 13 },
        { _id: 15, title: 'Обед', parent: 13 },
        { _id: 17, title: 'Второе блюдо', parent: 15 },
        { _id: 18, title: 'Ужин', parent: 13 },
        { _id: 19, title: 'Часть вторая', parent: 11 },
        { _id: 20, title: 'Часть третья', parent: 11 }

      ]
      
      const lists = _.filter(listsData, ['parent', null])

      let level = 0

      function getChildren(element) {
        const items = _.filter(listsData, ['parent', element._id])
        element.items = items
        element.level = level

        level++
        for (const item of items) {
          getChildren(item)
        }
        level--
      }

      for (const listItem of lists) {
        getChildren(listItem)
      }

      dispatch({ type: types.LOAD_SUCCESS, payload: lists })
      return lists
    }
  },
  update: (id, updatedProps) => {
    return async dispatch => {
      dispatch({ type: types.UPDATE_ITEM, payload: {
        id, updatedProps
      } })
    }
  },
}
