import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import i18n from 'meteor/universe:i18n'

import Main from './pages/Main'

i18n.setLocale('ru')

class App extends Component {
  render() {
    return (
      <Router>
        <Switch location={ location }>
          <Route path="/" component={ Main } />
        </Switch>
      </Router>
    )
  }
}

export default App