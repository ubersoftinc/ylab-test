import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'

import listsActions from '../../../../redux/actions/lists'

import './styles.less'

function mapStateToProps(state) {  
  return state
}

function mapDispatchToProps(dispatch) {  
  return {
    updateTitle: (id, updatedProps) => dispatch(listsActions.update(id, updatedProps)),
  }
}

class ListItem extends Component {
  static propTypes = {
    _id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    parent: PropTypes.number,
    items: PropTypes.array,
    level: PropTypes.number,
    updateTitle: PropTypes.func,
    setEditMode: PropTypes.func,
    onKeyPress: PropTypes.func,
    saveChanges: PropTypes.func,
    onChange: PropTypes.func,
  }

  static defaultProps = {
    parent: null,
    items: [],
    level: 1,
    updateTitle: () => {
      return new Promise((resolve, reject) => {})
    },
    setEditMode: () => {},
    onKeyPress: () => {},
    saveChanges: () => {},
    onChange: () => {},
  }

  constructor() {
    super()

    this.state = {
      isEdit: false,
      isError: false,
    }
  }

  render() {
    const { isEdit, isError } = this.state
    const { title, items, level, updateTitle, setEditMode, onKeyPress, saveChanges, onChange } = this.props

    return (
      <div className={ cn('ListItem', { 'ListItem_offset': level > 1 }) }>
        { isEdit ? (
          <input className={ cn('ListItem__input', { 'ListItem__input_error': isError }) } autoFocus type="text" defaultValue={ title } onBlur={ saveChanges.bind(this) } onKeyPress={ onKeyPress.bind(this) } onChange={ onChange.bind(this) } />
        ) : (
          <div className="ListItem__title" onClick={ setEditMode.bind(this) }>{ title }</div>
        ) }
        { items.map(item => (
          <ListItem key={ item._id } { ...item } updateTitle={ updateTitle } setEditMode={ setEditMode } saveChanges={ saveChanges } onKeyPress={ onKeyPress } onChange={ onChange } />
        )) }
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListItem)
