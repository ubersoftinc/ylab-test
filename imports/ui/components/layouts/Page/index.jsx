import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './styles.less'

class Page extends Component {
  static propTypes = {
    content: PropTypes.element,
  }

  static defaultProps = {
    content: null,
  }

  render() {
    return (
      <div className="Page">
        { this.props.content }
      </div>
    )
  }
}

export default Page