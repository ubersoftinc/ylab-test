import { combineReducers } from 'redux'

import listsRedusers from './lists'

export default combineReducers({ lists: listsRedusers })