import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import cn from 'classnames'

import Panel from '../Panel/index'
import ListItem from '../ListItem/index'

import listsActions from '../../../../redux/actions/lists'

import './styles.less'

function mapStateToProps(state) {  
  return state
}

function mapDispatchToProps(dispatch) {  
  return {
    updateTitle: (id, updatedProps) => dispatch(listsActions.update(id, updatedProps)),
  }
}

class List extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.array,
    updateTitle: PropTypes.func,
  }

  static defaultProps = {
    items: [],
    updateTitle: () => {
      return new Promise((resolve, reject) => {})
    },
  }

  constructor() {
    super()

    this.state = {
      isEdit: false,
      isError: false,
    }
  }

  setEditMode() {
    this.setState({
      isEdit: true,
    })
  }

  onKeyPress(event) {
    if (event.key === 'Enter') {
      this.saveChanges(event)
    }
  }

  onChange(event) {
    const title = event.target.value
    let isError
    
    isError = title.length <= 0 || title.length > 255
    
    this.setState({
      isError,
    })
  }

  saveChanges(event) {
    const { isError } = this.state

    if (isError) {
      return
    }

    const { _id, updateTitle } = this.props
    const title = event.target.value

    updateTitle(_id, {
      title,
    }).then(response => {
      this.setState({
        isEdit: false,
      })
    })
  }

  render() {
    const { isEdit, isError } = this.state
    const { title, items } = this.props

    return (
      <div>
        { isEdit ? (
          <input className={ cn('List__input', { 'List__input_error': isError }) } autoFocus type="text" defaultValue={ title } onBlur={ this.saveChanges.bind(this) } onKeyPress={ this.onKeyPress.bind(this) } onChange={ this.onChange.bind(this) } />
        ) : (
          <h1 className="List__title" onClick={ this.setEditMode.bind(this) }>{ title }</h1>
        ) }
        <Panel content={
          <div>
            { items.map(item => (
              <ListItem key={ item._id } { ...item } setEditMode={ this.setEditMode } saveChanges={ this.saveChanges } onKeyPress={ this.onKeyPress } onChange={ this.onChange } />
            )) }
          </div>
        } />
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)