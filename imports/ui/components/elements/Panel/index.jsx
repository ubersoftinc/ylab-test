import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './styles.less'

class Panel extends Component {
  static propTypes = {
    content: PropTypes.element,
  }

  static defaultProps = {
    content: null,
  }

  render() {
    return (
      <div className="Panel">
        { this.props.content }
      </div>
    )
  }
}

export default Panel