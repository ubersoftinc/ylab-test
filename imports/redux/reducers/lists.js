import { _ } from 'lodash'

import { types } from '../actions/lists'

const listsReducers = (state = { lists: [] }, action) => {
  switch (action.type) {
    case types.LOAD_SUCCESS:
      return { ...state, lists: action.payload }
    case types.UPDATE_ITEM:
      const lists = state.lists.splice(0)
      const { id, updatedProps } = action.payload
      
      function findItem(id, items) {
        for (const item of items) {
          if (item._id === id) {
            return item
          } else {
            const find = findItem(id, item.items)
            if (find) {
              return find
            }
          }
        }
      }
      
      const item = findItem(id, lists)
      _.merge(item, updatedProps)

      return { ...state, lists }
    default:
      return state
  }
}

export default listsReducers