import React, { Component } from 'react'
import { connect } from 'react-redux'
import { _ } from 'lodash'

import listsActions from '../../redux/actions/lists'

import Page from '../components/layouts/Page/index'
import List from '../components/elements/List/index'

function mapStateToProps(state) {  
  return { ...state.lists }
}

function mapDispatchToProps(dispatch) {  
  return {
    loadLists: () => dispatch(listsActions.load()),
  }
}

class Main extends Component {
  componentWillMount() {
    this.props.loadLists()
  }

  render() {
    const { lists } = this.props

    return (
      <Page content={
        <div>
          { lists.map(item => (
            <List key={ item._id } {...item} />
          )) }
        </div>
      } />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)